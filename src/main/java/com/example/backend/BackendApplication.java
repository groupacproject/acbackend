package com.example.backend;

import com.example.backend.Controller.user.picture.SearchController;
import com.example.backend.pojo.Picture;
import com.example.backend.service.impl.user.picture.SearchServiceImpl;
import com.example.backend.service.user.picture.SearchService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);

    }

}
