package com.example.backend.utils;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class PicExcel {
    private Integer id;
    private Integer userId;
    private String userName;
    private String title;
    private String type;
    private String photo;
    private String shotTime;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String submitTime;
    private String description;
    private String status;
    private String code;
    private String advice;
}
