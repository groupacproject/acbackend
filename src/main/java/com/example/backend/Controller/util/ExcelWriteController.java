package com.example.backend.Controller.util;

import com.example.backend.pojo.Picture;
import com.example.backend.service.user.excel.ExcelDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class ExcelWriteController {
    @Autowired
    private ExcelDownloadService excelDownloadService;


    @PostMapping("/user/excel/write/")
    @ResponseBody
    public Map<String, String> getexcel(@RequestParam("pic")List<Picture> data){
        System.out.println(data);
        return excelDownloadService.getexcel(data);

    }
}
