package com.example.backend.Controller.user.picture;

import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.utils.ResponseResult;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/file")
public class FileUploadController {


    @PostMapping("/upload/")
    public ResponseResult uploadFile(@RequestParam(value = "file",required = false) MultipartFile file){
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();
        Map<String, String> map = new HashMap<>();
        // 判断文件是否为空
        if(file.isEmpty()){
            return ResponseResult.fail();
        }
        // 获取传过来的文件名字
        String OriginalFilename=file.getOriginalFilename();
        // 为了防止重名覆盖，获取系统时间戳+原始文件的后缀名
        String fileName=System.currentTimeMillis()+"."+OriginalFilename.substring(OriginalFilename.lastIndexOf(".")+1);
        String sub = OriginalFilename.substring(OriginalFilename.lastIndexOf(".")+1);
        if(!sub.equals("img") && !sub.equals("png") && !sub.equals("jpg"))
        {
            System.out.println(sub);
            return ResponseResult.fail();
        }
        // 设置保存地址（这里是转义字符）
        //1.后台保存位置
        String path = "D:\\小学期\\acproject\\web\\src\\assets\\images\\";
        String pathBase = path + fileName;
        File dest=new File(path+fileName);
        // 判断文件是否存在
        if(!dest.getParentFile().exists()){
            // 不存在就创建一个
            dest.getParentFile().mkdirs();
        }
        try {
            // 后台上传
            file.transferTo(dest);
            return new ResponseResult(200, "文件上传成功", fileName);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseResult.fail();
        }


    }

}
