package com.example.backend.Controller.user.picture;


import com.example.backend.pojo.Picture;
import com.example.backend.service.user.picture.GetListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetListController {
    @Autowired
    private GetListService getListService;

    @GetMapping("/user/picture/getlist/")
    public List<Picture> getList(){
        return getListService.getList();
    }
}
