package com.example.backend.Controller.user.admin;

import com.example.backend.pojo.Picture;
import com.example.backend.service.impl.user.admin.AdminGetListServiceImpl;
import com.example.backend.service.user.admin.AdminGetListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class AdminGetListController {
    @Autowired
    private AdminGetListService adminGetListService;

    @GetMapping("/user/admin/getlist/")
    public List<Picture> admingetlist(@RequestParam Map<String, String> data){
        return adminGetListService.admingetlist(data);
    }
}
