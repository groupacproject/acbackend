package com.example.backend.Controller.user.admin;

import com.example.backend.pojo.Picture;
import com.example.backend.service.user.admin.AdminGetListService;
import com.example.backend.service.user.admin.AdminSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class AdminSearchController {
    @Autowired
    private AdminSearchService adminSearchService;

    @GetMapping("/user/admin/search/")
    public List<Picture> adminsearch(@RequestParam Map<String, String> data) {
        return adminSearchService.adminsearch(data);
    }
}
