package com.example.backend.Controller.user.picture;

import com.example.backend.service.user.picture.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class UpdateController {
    @Autowired
    private UpdateService updateService;

    @PostMapping("/user/picture/update/")
    public Map<String, String> update(@RequestParam Map<String, String> data){
        return updateService.update(data);
    }
}
