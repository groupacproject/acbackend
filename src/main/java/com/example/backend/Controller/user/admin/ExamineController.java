package com.example.backend.Controller.user.admin;

import com.example.backend.service.user.admin.ExamineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ExamineController {
    @Autowired
    private ExamineService examineService;

    @PostMapping("/user/admin/examine/")
    public Map<String, String> examine(@RequestParam Map<String, String> data){
        return examineService.examine(data);
    }
}
