package com.example.backend.Controller.user.picture;

import com.example.backend.pojo.Picture;
import com.example.backend.service.user.picture.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class SearchController {
    @Autowired
    private SearchService searchService;

    @GetMapping("/user/picture/search/")
    public List<Picture> search(@RequestParam Map<String, String> data){
        return searchService.search(data);
    }
}
