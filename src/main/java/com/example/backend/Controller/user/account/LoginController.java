package com.example.backend.Controller.user.account;

import com.example.backend.pojo.User;
import com.example.backend.service.user.account.LoginService;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.SessionException;
import java.awt.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



@Controller
@RestController
public class LoginController {

    String text;

    private static final String SESSION_KEY = "captcha";
    @Autowired
    private LoginService loginService;
    @Autowired
    private HttpServletRequest request;

    @Autowired(required = false)
    private HttpServletResponse response;

    @RequestMapping("/user/account/captcha/")
    public void easyCaptcha() throws IOException {

        Map<String, String> map = new HashMap<>();



        // 三个参数分别为宽、高、位数
        SpecCaptcha specCaptcha = new SpecCaptcha(130, 48, 5);
        // 设置字体
        specCaptcha.setFont(new Font("Verdana", Font.PLAIN, 32));  // 有默认字体，可以不用设置
        // 设置类型，纯数字、纯字母、字母数字混合
        specCaptcha.setCharType(Captcha.TYPE_ONLY_NUMBER);

        // 验证码存入session
        request.getSession().setAttribute("captcha", specCaptcha.text().toLowerCase());

        // 输出图片流
        specCaptcha.out(response.getOutputStream());
        text = specCaptcha.text();
        System.out.println("y:" + text);
    }


    @PostMapping("/user/account/token/")
    public Map<String, String> getToken(@RequestParam Map<String, String> map){
        String username = map.get("username");
        String password = map.get("password");
        String admin = map.get("admin");
        String verCode = map.get("verCode");
        System.out.println("X:" + text);
        System.out.println("Z:" + verCode);
        if(!(verCode.trim().toLowerCase().equals(text)))
        {

            map.put("error_message", "验证码错误");
            return map;
        }

        return loginService.getToken(username, password, admin);
    }



}
