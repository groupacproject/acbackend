package com.example.backend.service.impl.user.admin;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.backend.mapper.PictureMapper;
import com.example.backend.pojo.Picture;
import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.service.user.admin.ExamineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

@Service
public class ExamineServiceImpl implements ExamineService {

    @Autowired
    private PictureMapper pictureMapper;
    @Override
    public Map<String, String> examine(Map<String, String> data) {
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();
        Map<String, String> map = new HashMap<>();

        String status = data.get("status");
        String advice = data.get("advice");
        int picture_id = Integer.parseInt(data.get("picture_id"));
        Picture picture = pictureMapper.selectById(picture_id);
        String code = data.get("code");
        if(!user.getAdmin().equals("true"))
        {
            map.put("error_message", "无访问权限");
            return map;
        }
        if(status.equals("通过")){
            code = generateRandomString(30);
            advice = "恭喜你审核通过";
        }
        if(advice == null || advice.length() == 0){
            advice = "审核员未给出审核建议";
        }
        if(status.equals("未通过") || status.equals("审核中")){
            code = "未生成";
        }
        Picture new_picture = new Picture(
                picture.getId(),
                picture.getUserId(),
                picture.getUserName(),
                picture.getTitle(),
                picture.getType(),
                picture.getPhoto(),
                picture.getShotTime(),
                picture.getSubmitTime(),
                picture.getDescription(),
                status,
                code,
                advice
        );
        pictureMapper.updateById(new_picture);
        map.put("error_message", "success");
        return map;
    }
    public static String generateRandomString(int length) {
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(chars.length());
            sb.append(chars.charAt(index));
        }
        return sb.toString();
    }

}
