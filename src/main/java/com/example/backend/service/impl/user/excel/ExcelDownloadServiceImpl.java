package com.example.backend.service.impl.user.excel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.example.backend.pojo.Picture;
import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.service.user.excel.ExcelDownloadService;
import com.example.backend.utils.PicExcel;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExcelDownloadServiceImpl implements ExcelDownloadService {

    @Override
    public Map<String, String> getexcel(List<Picture> data) {
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();
        Map<String, String> map = new HashMap<>();

        List<Picture> list = new ArrayList<>();
        ExcelWriterBuilder writePic = EasyExcel.write("test1.xlsx", PicExcel.class);
        ExcelWriterSheetBuilder sheet = writePic.sheet();
        sheet.doWrite(data);
        System.out.println(data);
        map.put("error_message ","success");
        return map;
    }
}
