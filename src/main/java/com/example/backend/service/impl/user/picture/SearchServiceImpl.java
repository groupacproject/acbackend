package com.example.backend.service.impl.user.picture;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.backend.mapper.PictureMapper;
import com.example.backend.pojo.Picture;
import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.service.user.picture.SearchService;
import com.example.backend.utils.PicExcel;
import com.sun.org.apache.xpath.internal.objects.XNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private PictureMapper pictureMapper;
    @Override
    public List<Picture> search(Map<String, String> data) {
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();

        List<Picture> list = new ArrayList<>();
        String title = data.get("title");
        String type = data.get("type");
        String year = data.get("year");
        String month =data.get("month");
        String date = data.get("date");
        String excel = data.get("excel");
        String submitTime = new String();
        int temp = 0;
        Map<String, String> map = new HashMap<>();

        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", user.getId());

        if(title != null && title.length() != 0)
        {
            queryWrapper.eq("title", title);
            temp++;
        }
        if(type != null && type.length() != 0)
        {
            queryWrapper.eq("type", type);
            temp++;
        }
        if((year != null && year.length() != 0) && (month != null && month.length() != 0) && (date != null && date.length() != 0)){
            queryWrapper.eq("submit_time", year + "-" + month + "-" + date);
            temp++;
        } else if ((year == null || year.length() == 0) && (month == null || month.length() == 0) && (date == null || date.length() == 0)) {

        }else {

        }
        if(temp == 0)
        {
            return list;
        }

        if(excel != "1")
        {
            ExcelWriterBuilder writePic = EasyExcel.write("D:\\小学期\\acproject\\web\\src\\assets\\excel\\test1.xlsx", PicExcel.class);
            ExcelWriterSheetBuilder sheet = writePic.sheet();
            sheet.doWrite(pictureMapper.selectList(queryWrapper));
        }

        return pictureMapper.selectList(queryWrapper);
    }
}
