package com.example.backend.service.impl.user.admin;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.backend.mapper.PictureMapper;
import com.example.backend.pojo.Picture;
import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.service.user.admin.AdminSearchService;
import com.example.backend.utils.PicExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminSearchServiceImpl implements AdminSearchService {
    @Autowired
    PictureMapper pictureMapper;

    @Override
    public List<Picture> adminsearch(Map<String, String> data) {
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();

        Map<String, String> map = new HashMap<>();
        if(!user.getAdmin().equals("true"))
        {
            map.put("error_message", "无访问权限");
            return null;
        }
        List<Picture> list = new ArrayList<>();
        String title = data.get("title");
        String type = data.get("type");
        String year = data.get("year");
        String month =data.get("month");
        String date = data.get("date");
        String user_name = data.get("user_name");
        String excel = data.get("excel");
        int temp = 0;

        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();

        if(user_name != null && user_name.length() != 0)
        {
            queryWrapper.eq("user_name", user_name);
            temp++;
        }

        if(title != null && title.length() != 0)
        {
            queryWrapper.eq("title", title);
            temp++;
        }
        if(type != null && type.length() != 0)
        {
            queryWrapper.eq("type", type);
            temp++;
        }
        if((year != null && year.length() != 0) && (month != null && month.length() != 0) && (date != null && date.length() != 0)){
            queryWrapper.eq("submit_time", year + "-" + month + "-" + date);
            temp++;
        } else if ((year == null || year.length() == 0) && (month == null || month.length() == 0) && (date == null || date.length() == 0)) {

        }else {

        }
        if(temp == 0)
        {
            return list;
        }
        if(excel != "1")
        {
            ExcelWriterBuilder writePic = EasyExcel.write("D:\\小学期\\acproject\\web\\src\\assets\\excel\\test1.xlsx", PicExcel.class);
            ExcelWriterSheetBuilder sheet = writePic.sheet();
            sheet.doWrite(pictureMapper.selectList(queryWrapper));
        }


        return pictureMapper.selectList(queryWrapper);
    }
}
