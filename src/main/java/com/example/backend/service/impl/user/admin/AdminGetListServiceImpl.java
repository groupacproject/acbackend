package com.example.backend.service.impl.user.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.backend.mapper.PictureMapper;
import com.example.backend.pojo.Picture;
import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.service.user.admin.AdminGetListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminGetListServiceImpl implements AdminGetListService {
    @Autowired
    PictureMapper pictureMapper;
    @Override
    public List<Picture> admingetlist(Map<String, String> data) {
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();

        Map<String, String> map = new HashMap<>();
        if(!user.getAdmin().equals("true"))
        {
            map.put("error_message", "无访问权限");
            return null;
        }

        String status = data.get("code_status");
        if(status == null || status.length() == 0){
            return pictureMapper.selectList(null);
        }
        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", status);
        return pictureMapper.selectList(queryWrapper);
    }
}
