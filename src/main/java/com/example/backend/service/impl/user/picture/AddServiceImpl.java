package com.example.backend.service.impl.user.picture;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.backend.Controller.user.picture.FileUploadController;
import com.example.backend.Controller.user.picture.UpdateController;
import com.example.backend.mapper.PictureMapper;
import com.example.backend.pojo.Picture;
import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.service.user.picture.AddService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AddServiceImpl implements AddService {


    @Autowired
    private PictureMapper pictureMapper;

    @Override
    public Map<String, String> add(Map<String, String> data) {
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();


        String title = data.get("title");
        String type = data.get("type");
        String description = data.get("description");
        String photo = data.get("photo");
        String shotTime = data.get("shotTime");

        Map<String, String> map = new HashMap<>();

        if(title == null || title.length() == 0){
            map.put("error_message", "标题不能为空");
            return map;
        }
        if(title.length() > 8)
        {
            map.put("error_message", "作品名称不能大于8个字");
            return map;
        }
        if(description.length() > 1000)
        {
            map.put("error_message", "描述不能大于100字");
            return map;
        }
        if(description == null || description.length() == 0)
        {
            description = "无描述";
        }
        if(type == null || type.length() == 0)
        {
            map.put("error_message", "类型不能为空");
            return map;
        }
        if(type.length() > 6)
        {
            map.put("error_message", "类型不能6个字");
            return map;
        }
        if(photo == null || photo.length() == 0)
        {
            map.put("error_message", "请上传图片");
            return map;
        }
        if(shotTime == null || shotTime.length() == 0)
        {
            shotTime = "无拍摄时间";
        }
        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("title", title);
        List<Picture> pictures = pictureMapper.selectList(queryWrapper);
        if(!pictures.isEmpty()){
            map.put("error_message", "作品名字已存在");
            return map;
        }

        String code = "未生成";
        String status = "审核中";
        String advice = "审核员还未给出建议";
        Date now = new Date(System.currentTimeMillis());
        title= title.trim();

        Picture picture = new Picture(null, user.getId(), user.getUsername(), title, type, photo, shotTime, now.toString(), description, status, code, advice);


        pictureMapper.insert(picture);
        map.put("error_message", "success");

        return map;
    }
}
