package com.example.backend.service.impl.user.picture;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.backend.mapper.PictureMapper;
import com.example.backend.pojo.Picture;
import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.service.user.picture.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class UpdateServiceImpl implements UpdateService {

    @Autowired
    private PictureMapper pictureMapper;
    @Override
    public Map<String, String> update(Map<String, String> data) {
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();

        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();

        int picture_id = Integer.parseInt(data.get("picture_id"));

        String title = data.get("title");
        String type = data.get("type");
        String description = data.get("description");
        String photo = data.get("photo");
        String shotTime = data.get("shotTime");
        String status = new String();
        String advice = new String();
        String code = new String();
        Map<String, String> map = new HashMap<>();



        if(user.getAdmin() == "false")
        {
            //实现获取

        }
        if(code == null || code.length() == 0)
        {
            code = "未生成";
        }
        if(status == null || status.length() == 0)
        {
            status = "未审核";
        }
        if(advice == null || advice.length() == 0)
        {
            advice = "审核员还未给出建议";
        }
        if(advice.length() > 100)
        {
            map.put("error_message", "建议不能超过100字");
            return map;
        }
        if(title == null || title.length() == 0){
            map.put("error_message", "标题不能为空");
            return map;
        }
        if(title.length() > 10)
        {
            map.put("error_message", "标题不能大于10字");
            return map;
        }
        if(description.length() > 1000)
        {
            map.put("error_message", "描述不能大于100字");
            return map;
        }
        if(description == null || description.length() == 0)
        {
            description = "无描述";
        }
        if(type == null || type.length() == 0)
        {
            map.put("error_message", "类型不能为空");
            return map;
        }
        if(type.length() > 100)
        {
            map.put("error_message", "类型不能大于100字");
            return map;
        }

        if(shotTime == null || shotTime.length() == 0)
        {
            shotTime = "无拍摄时间";
        }

        Picture picture = pictureMapper.selectById(picture_id);

        if(picture == null){
            map.put("error_message", "图片不存在或已被删除");
            return map;
        }

        if(!picture.getUserId().equals(user.getId())){
            map.put("error_message", "没有修改权限");
            return map;
        }
        title= title.trim();

        java.sql.Date now = new Date(System.currentTimeMillis());
        Picture new_picture = new Picture(
                picture.getId(),
                user.getId(),
                user.getUsername(),
                title,
                type,
                photo,
                shotTime,
                now.toString(),
                description,
                status,
                code,
                advice
        );

        pictureMapper.updateById(new_picture);

        map.put("error_message", "success");

        return map;
    }
}
