package com.example.backend.service.impl.user.picture;


import com.example.backend.mapper.PictureMapper;
import com.example.backend.pojo.Picture;
import com.example.backend.pojo.User;
import com.example.backend.service.impl.utils.UserDetailsImpl;
import com.example.backend.service.user.picture.RemoveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class RemoveServiceImpl implements RemoveService {

    @Autowired
    private PictureMapper pictureMapper;


    @Override
    public Map<String, String> remove(Map<String, String> data) {
        UsernamePasswordAuthenticationToken authenticationToken =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl loginUser = (UserDetailsImpl) authenticationToken.getPrincipal();
        User user = loginUser.getUser();

        int picture_id = Integer.parseInt(data.get("picture_id"));
        Picture picture = pictureMapper.selectById(picture_id);

        Map<String, String> map = new HashMap<>();

        if(picture == null){
            map.put("error_message", "图片不存在或已被删除");
            return map;
        }
        if(!picture.getUserId().equals(user.getId())){
            map.put("error_message", "没有权限删除");
            return map;
        }

        pictureMapper.deleteById(picture_id);
        map.put("error_message", "success");

        return map;
    }
}
