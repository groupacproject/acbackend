package com.example.backend.service.user.picture;

import com.example.backend.pojo.Picture;

import java.util.List;
import java.util.Map;

public interface SearchService {
    List<Picture> search(Map<String, String> data);
}
