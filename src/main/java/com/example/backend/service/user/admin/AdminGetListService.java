package com.example.backend.service.user.admin;

import com.example.backend.pojo.Picture;

import java.util.List;
import java.util.Map;

public interface AdminGetListService {
    public List<Picture> admingetlist(Map<String, String> data);

}
