package com.example.backend.service.user.picture;

import java.util.Map;

public interface AddService {
    Map<String, String> add(Map<String, String> data);
}
