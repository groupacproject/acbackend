package com.example.backend.service.user.picture;

import com.example.backend.pojo.Picture;

import java.util.List;

public interface GetListService {

    List<Picture> getList();
}
