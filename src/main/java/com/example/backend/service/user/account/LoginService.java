package com.example.backend.service.user.account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface LoginService {
    public Map<String, String> getToken(String username, String password, String admin);

}
