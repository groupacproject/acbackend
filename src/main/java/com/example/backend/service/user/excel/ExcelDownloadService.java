package com.example.backend.service.user.excel;

import com.example.backend.pojo.Picture;

import java.util.List;
import java.util.Map;

public interface ExcelDownloadService {
    public Map<String, String> getexcel(List<Picture> data);
}
