package com.example.backend.service.user.admin;

import java.util.Map;

public interface ExamineService {
    public Map<String, String> examine(Map<String, String> data);
}
